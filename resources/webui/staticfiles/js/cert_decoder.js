/* Base64 JavaScript decoder
 Copyright (c) 2008-2020 Lapo Luchini <lapo@lapo.it>

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var Base64 = {},
    decoder,
    haveU8 = (typeof Uint8Array == 'function');

Base64.decode = function (a) {
    var isString = (typeof a == 'string');
    var i;
    if (decoder === undefined) {
        var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
            ignore = "= \f\n\r\t\u00A0\u2028\u2029";
        decoder = [];
        for (i = 0; i < 64; ++i)
            decoder[b64.charCodeAt(i)] = i;
        for (i = 0; i < ignore.length; ++i)
            decoder[ignore.charCodeAt(i)] = -1;
        decoder['-'.charCodeAt(0)] = decoder['+'.charCodeAt(0)];
        decoder['_'.charCodeAt(0)] = decoder['/'.charCodeAt(0)];
    }
    var out = haveU8 ? new Uint8Array(a.length * 3 >> 2) : [];
    var bits = 0, char_count = 0, len = 0;
    for (i = 0; i < a.length; ++i) {
        var c = isString ? a.charCodeAt(i) : a[i];
        if (c == 61)
            break;
        c = decoder[c];
        if (c == -1)
            continue;
        if (c === undefined)
            throw 'Illegal character at offset ' + i;
        bits |= c;
        if (++char_count >= 4) {
            out[len++] = (bits >> 16);
            out[len++] = (bits >> 8) & 0xFF;
            out[len++] = bits & 0xFF;
            bits = 0;
            char_count = 0;
        } else {
            bits <<= 6;
        }
    }
    switch (char_count) {
    case 1:
        throw "Base64 encoding incomplete: at least 2 bits missing";
    case 2:
        out[len++] = (bits >> 10);
        break;
    case 3:
        out[len++] = (bits >> 16);
        out[len++] = (bits >> 8) & 0xFF;
        break;
    }
    if (haveU8 && out.length > len)
        out = out.subarray(0, len);
    return out;
};

Base64.pretty = function (str) {
    if (str.length % 4 > 0)
        str = (str + '===').slice(0, str.length + str.length % 4);
    str = str.replace(/-/g, '+').replace(/_/g, '/');
    return str.replace(/(.{80})/g, '$1\n');
};

Base64.re = /-----BEGIN [^-]+-----([A-Za-z0-9+/=\s]+)-----END [^-]+-----|begin-base64[^\n]+\n([A-Za-z0-9+/=\s]+)====/;
Base64.unarmor = function (a) {
    var m = Base64.re.exec(a);
    if (m) {
        if (m[1])
            a = m[1];
        else if (m[2])
            a = m[2];
        else
            throw "RegExp out of sync";
    }
    return Base64.decode(a);
};

var max = 10000000000000;

/**
 * Arbitrary length base-10 value.
 * @param {number} value - Optional initial value (will be 0 otherwise).
 */
function Int10(value) {
    this.buf = [+value || 0];
}

/**
 * Multiply value by m and add c.
 * @param {number} m - multiplier, must be < =256
 * @param {number} c - value to add
 */
Int10.prototype.mulAdd = function (m, c) {
    var b = this.buf,
        l = b.length,
        i, t;
    for (i = 0; i < l; ++i) {
        t = b[i] * m + c;
        if (t < max)
            c = 0;
        else {
            c = 0|(t / max);
            t -= c * max;
        }
        b[i] = t;
    }
    if (c > 0)
        b[i] = c;
};

/**
 * Subtract value.
 * @param {number} c - value to subtract
 */
Int10.prototype.sub = function (c) {
    var b = this.buf,
        l = b.length,
        i, t;
    for (i = 0; i < l; ++i) {
        t = b[i] - c;
        if (t < 0) {
            t += max;
            c = 1;
        } else
            c = 0;
        b[i] = t;
    }
    while (b[b.length - 1] === 0)
        b.pop();
};

/**
 * Convert to decimal string representation.
 * @param {*} base - optional value, only value accepted is 10
 */
Int10.prototype.toString = function (base) {
    if ((base || 10) != 10)
        throw 'only base 10 is supported';
    var b = this.buf,
        s = b[b.length - 1].toString();
    for (var i = b.length - 2; i >= 0; --i)
        s += (max + b[i]).toString().substring(1);
    return s;
};

/**
 * Convert to Number value representation.
 * Will probably overflow 2^53 and thus become approximate.
 */
Int10.prototype.valueOf = function () {
    var b = this.buf,
        v = 0;
    for (var i = b.length - 1; i >= 0; --i)
        v = v * max + b[i];
    return v;
};

/**
 * Return value as a simple Number (if it is <= 10000000000000), or return this.
 */
Int10.prototype.simplify = function () {
    var b = this.buf;
    return (b.length == 1) ? b[0] : this;
};

/* Converted from: https://www.cs.auckland.ac.nz/~pgut001/dumpasn1.cfg
 which is made by Peter Gutmann and whose license states:
 You can use this code in whatever way you want,
 as long as you don't try to claim you wrote it. */

function get_oids () {
return {'0.4.0.1862': 'etsiQcsProfile', '0.4.0.1862.1': 'etsiQcs', '0.4.0.1862.1.1': 'etsiQcsCompliance', '0.4.0.1862.1.2': 'etsiQcsLimitValue', '0.4.0.1862.1.3': 'etsiQcsRetentionPeriod', '0.4.0.1862.1.4': 'etsiQcsQcSSCD', '0.9.2342.19200300.100.1.1': 'userID', '0.9.2342.19200300.100.1.3': 'rfc822Mailbox', '0.9.2342.19200300.100.1.25': 'domainComponent', '1.0.10118.3.0.49': 'ripemd160', '1.0.10118.3.0.50': 'ripemd128', '1.0.10118.3.0.55': 'whirlpool', '1.0.18033.2': 'iso18033-2', '1.0.18033.2.2': 'kem', '1.0.18033.2.2.4': 'kemRSA', '1.2.752.34.1': 'seis-cp', '1.2.752.34.1.1': 'SEIS high-assurance policyIdentifier', '1.2.752.34.1.2': 'SEIS GAK policyIdentifier', '1.2.752.34.2': 'SEIS pe', '1.2.752.34.3': 'SEIS at', '1.2.752.34.3.1': 'SEIS at-personalIdentifier', '1.2.840.10040.1': 'module', '1.2.840.10040.1.1': 'x9f1-cert-mgmt', '1.2.840.10040.2': 'holdinstruction', '1.2.840.10040.2.1': 'holdinstruction-none', '1.2.840.10040.2.2': 'callissuer', '1.2.840.10040.2.3': 'reject', '1.2.840.10040.2.4': 'pickupToken', '1.2.840.10040.3': 'attribute', '1.2.840.10040.3.1': 'countersignature', '1.2.840.10040.3.2': 'attribute-cert', '1.2.840.10040.4': 'algorithm', '1.2.840.10040.4.1': 'dsa', '1.2.840.10040.4.2': 'dsa-match', '1.2.840.10040.4.3': 'dsaWithSha1', '1.2.840.10045.1': 'fieldType', '1.2.840.10045.1.1': 'prime-field', '1.2.840.10045.1.2': 'characteristic-two-field', '1.2.840.10045.1.2.3': 'characteristic-two-basis', '1.2.840.10045.1.2.3.1': 'onBasis', '1.2.840.10045.1.2.3.2': 'tpBasis', '1.2.840.10045.1.2.3.3': 'ppBasis', '1.2.840.10045.2': 'publicKeyType', '1.2.840.10045.2.1': 'ecPublicKey', '1.2.840.10045.3.0.1': 'c2pnb163v1', '1.2.840.10045.3.0.2': 'c2pnb163v2', '1.2.840.10045.3.0.3': 'c2pnb163v3', '1.2.840.10045.3.0.5': 'c2tnb191v1', '1.2.840.10045.3.0.6': 'c2tnb191v2', '1.2.840.10045.3.0.7': 'c2tnb191v3', '1.2.840.10045.3.0.10': 'c2pnb208w1', '1.2.840.10045.3.0.11': 'c2tnb239v1', '1.2.840.10045.3.0.12': 'c2tnb239v2', '1.2.840.10045.3.0.13': 'c2tnb239v3', '1.2.840.10045.3.0.16': 'c2pnb272w1', '1.2.840.10045.3.0.18': 'c2tnb359v1', '1.2.840.10045.3.0.19': 'c2pnb368w1', '1.2.840.10045.3.0.20': 'c2tnb431r1', '1.2.840.10045.3.1.1': 'prime192v1', '1.2.840.10045.3.1.2': 'prime192v2', '1.2.840.10045.3.1.3': 'prime192v3', '1.2.840.10045.3.1.4': 'prime239v1', '1.2.840.10045.3.1.5': 'prime239v2', '1.2.840.10045.3.1.6': 'prime239v3', '1.2.840.10045.3.1.7': 'prime256v1', '1.2.840.10045.4.1': 'ecdsaWithSHA1', '1.2.840.10045.4.2': 'ecdsaWithRecommended', '1.2.840.10045.4.3': 'ecdsaWithSpecified', '1.2.840.10045.4.3.1': 'ecdsaWithSHA224', '1.2.840.10045.4.3.2': 'ecdsaWithSHA256', '1.2.840.10045.4.3.3': 'ecdsaWithSHA384', '1.2.840.10045.4.3.4': 'ecdsaWithSHA512', '1.2.840.10046.1': 'fieldType', '1.2.840.10046.1.1': 'gf-prime', '1.2.840.10046.2': 'numberType', '1.2.840.10046.2.1': 'dhPublicKey', '1.2.840.10046.3': 'scheme', '1.2.840.10046.3.1': 'dhStatic', '1.2.840.10046.3.2': 'dhEphem', '1.2.840.10046.3.3': 'dhHybrid1', '1.2.840.10046.3.4': 'dhHybrid2', '1.2.840.10046.3.5': 'mqv2', '1.2.840.10046.3.6': 'mqv1', '1.2.840.10065.2.2': '?', '1.2.840.10065.2.3': 'healthcareLicense', '1.2.840.10065.2.3.1.1': 'license?', '1.2.840.10070': 'iec62351', '1.2.840.10070.8': 'iec62351_8', '1.2.840.10070.8.1': 'iecUserRoles', '1.2.840.113533.7': 'nsn', '1.2.840.113533.7.65': 'nsn-ce', '1.2.840.113533.7.65.0': 'entrustVersInfo', '1.2.840.113533.7.66': 'nsn-alg', '1.2.840.113533.7.66.3': 'cast3CBC', '1.2.840.113533.7.66.10': 'cast5CBC', '1.2.840.113533.7.66.11': 'cast5MAC', '1.2.840.113533.7.66.12': 'pbeWithMD5AndCAST5-CBC', '1.2.840.113533.7.66.13': 'passwordBasedMac', '1.2.840.113533.7.67': 'nsn-oc', '1.2.840.113533.7.67.0': 'entrustUser', '1.2.840.113533.7.68': 'nsn-at', '1.2.840.113533.7.68.0': 'entrustCAInfo', '1.2.840.113533.7.68.10': 'attributeCertificate', '1.2.840.113549.1.1': 'pkcs-1', '1.2.840.113549.1.1.1': 'rsaEncryption', '1.2.840.113549.1.1.2': 'md2WithRSAEncryption', '1.2.840.113549.1.1.3': 'md4WithRSAEncryption', '1.2.840.113549.1.1.4': 'md5WithRSAEncryption', '1.2.840.113549.1.1.5': 'sha1WithRSAEncryption', '1.2.840.113549.1.1.7': 'rsaOAEP', '1.2.840.113549.1.1.8': 'pkcs1-MGF', '1.2.840.113549.1.1.9': 'rsaOAEP-pSpecified', '1.2.840.113549.1.1.10': 'rsaPSS', '1.2.840.113549.1.1.11': 'sha256WithRSAEncryption', '1.2.840.113549.1.1.12': 'sha384WithRSAEncryption', '1.2.840.113549.1.1.13': 'sha512WithRSAEncryption', '1.2.840.113549.1.1.14': 'sha224WithRSAEncryption', '1.2.840.113549.1.1.6': 'rsaOAEPEncryptionSET', '1.2.840.113549.1.2': 'bsafeRsaEncr', '1.2.840.113549.1.3': 'pkcs-3', '1.2.840.113549.1.3.1': 'dhKeyAgreement', '1.2.840.113549.1.5': 'pkcs-5', '1.2.840.113549.1.5.1': 'pbeWithMD2AndDES-CBC', '1.2.840.113549.1.5.3': 'pbeWithMD5AndDES-CBC', '1.2.840.113549.1.5.4': 'pbeWithMD2AndRC2-CBC', '1.2.840.113549.1.5.6': 'pbeWithMD5AndRC2-CBC', '1.2.840.113549.1.5.9': 'pbeWithMD5AndXOR', '1.2.840.113549.1.5.10': 'pbeWithSHAAndDES-CBC', '1.2.840.113549.1.5.12': 'pkcs5PBKDF2', '1.2.840.113549.1.5.13': 'pkcs5PBES2', '1.2.840.113549.1.5.14': 'pkcs5PBMAC1', '1.2.840.113549.1.7': 'pkcs-7', '1.2.840.113549.1.7.1': 'data', '1.2.840.113549.1.7.2': 'signedData', '1.2.840.113549.1.7.3': 'envelopedData', '1.2.840.113549.1.7.4': 'signedAndEnvelopedData', '1.2.840.113549.1.7.5': 'digestedData', '1.2.840.113549.1.7.6': 'encryptedData', '1.2.840.113549.1.7.7': 'dataWithAttributes', '1.2.840.113549.1.7.8': 'encryptedPrivateKeyInfo', '1.2.840.113549.1.9': 'pkcs-9', '1.2.840.113549.1.9.1': 'emailAddress', '1.2.840.113549.1.9.2': 'unstructuredName', '1.2.840.113549.1.9.3': 'contentType', '1.2.840.113549.1.9.4': 'messageDigest', '1.2.840.113549.1.9.5': 'signingTime', '1.2.840.113549.1.9.6': 'countersignature', '1.2.840.113549.1.9.7': 'challengePassword', '1.2.840.113549.1.9.8': 'unstructuredAddress', '1.2.840.113549.1.9.9': 'extendedCertificateAttributes', '1.2.840.113549.1.9.10': 'issuerAndSerialNumber', '1.2.840.113549.1.9.11': 'passwordCheck', '1.2.840.113549.1.9.12': 'publicKey', '1.2.840.113549.1.9.13': 'signingDescription', '1.2.840.113549.1.9.14': 'extensionRequest', '1.2.840.113549.1.9.15': 'sMIMECapabilities', '1.2.840.113549.1.9.15.1': 'preferSignedData', '1.2.840.113549.1.9.15.2': 'canNotDecryptAny', '1.2.840.113549.1.9.15.3': 'receiptRequest', '1.2.840.113549.1.9.15.4': 'receipt', '1.2.840.113549.1.9.15.5': 'contentHints', '1.2.840.113549.1.9.15.6': 'mlExpansionHistory', '1.2.840.113549.1.9.16': 'id-sMIME', '1.2.840.113549.1.9.16.0': 'id-mod', '1.3.12.2.1011.7.1': 'decEncryptionAlgorithm', '1.3.12.2.1011.7.1.2': 'decDEA', '1.3.12.2.1011.7.2': 'decHashAlgorithm', '1.3.12.2.1011.7.2.1': 'decMD2', '1.3.12.2.1011.7.2.2': 'decMD4', '1.3.12.2.1011.7.3': 'decSignatureAlgorithm', '1.3.12.2.1011.7.3.1': 'decMD2withRSA', '1.3.12.2.1011.7.3.2': 'decMD4withRSA', '1.3.12.2.1011.7.3.3': 'decDEAMAC', '1.3.14.2.26.5': 'sha', '1.3.14.3.2.1.1': 'rsa', '1.3.14.3.2.2': 'md4WitRSA', '1.3.14.3.2.3': 'md5WithRSA', '1.3.14.3.2.4': 'md4WithRSAEncryption', '1.3.14.3.2.2.1': 'sqmod-N', '1.3.14.3.2.3.1': 'sqmod-NwithRSA', '1.3.14.3.2.6': 'desECB', '1.3.14.3.2.7': 'desCBC', '1.3.14.3.2.8': 'desOFB', '1.3.14.3.2.9': 'desCFB', '1.3.14.3.2.10': 'desMAC', '1.3.14.3.2.11': 'rsaSignature', '1.3.14.3.2.12': 'dsa', '1.3.14.3.2.13': 'dsaWithSHA', '1.3.14.3.2.14': 'mdc2WithRSASignature', '1.3.14.3.2.15': 'shaWithRSASignature', '1.3.14.3.2.16': 'dhWithCommonModulus', '1.3.14.3.2.17': 'desEDE', '1.3.14.3.2.18': 'sha', '1.3.14.3.2.19': 'mdc-2', '1.3.14.3.2.20': 'dsaCommon', '1.3.14.3.2.21': 'dsaCommonWithSHA', '1.3.14.3.2.22': 'rsaKeyTransport', '1.3.14.3.2.23': 'keyed-hash-seal', '1.3.14.3.2.24': 'md2WithRSASignature', '1.3.14.3.2.25': 'md5WithRSASignature', '1.3.14.3.2.26': 'sha1', '1.3.14.3.2.27': 'dsaWithSHA1', '1.3.14.3.2.28': 'dsaWithCommonSHA1', '1.3.14.3.2.29': 'sha-1WithRSAEncryption', '1.3.14.3.3.1': 'simple-strong-auth-mechanism', '1.3.14.7.2.1.1': 'ElGamal', '1.3.14.7.2.3.1': 'md2WithRSA', '1.3.14.7.2.3.2': 'md2WithElGamal', '1.3.36.1': 'document', '1.3.36.1.1': 'finalVersion', '1.3.36.1.2': 'draft', '1.3.36.2': 'sio', '1.3.36.2.1': 'sedu', '1.3.36.3': 'algorithm', '1.3.36.3.1': 'encryptionAlgorithm', '1.3.36.3.1.1': 'des', '1.3.36.3.1.1.1': 'desECB_pad', '1.3.36.3.1.1.1.1': 'desECB_ISOpad', '1.3.36.3.1.1.2.1': 'desCBC_pad', '1.3.36.3.1.1.2.1.1': 'desCBC_ISOpad', '1.3.36.3.1.3': 'des_3', '1.3.36.3.1.3.1.1': 'des_3ECB_pad', '1.3.36.3.1.3.1.1.1': 'des_3ECB_ISOpad', '1.3.36.3.1.3.2.1': 'des_3CBC_pad', '1.3.36.3.1.3.2.1.1': 'des_3CBC_ISOpad', '1.3.36.3.1.2': 'idea', '1.3.36.3.1.2.1': 'ideaECB', '1.3.36.3.1.2.1.1': 'ideaECB_pad', '1.3.36.3.1.2.1.1.1': 'ideaECB_ISOpad', '1.3.36.3.1.2.2': 'ideaCBC', '1.3.36.3.1.2.2.1': 'ideaCBC_pad', '1.3.36.3.1.2.2.1.1': 'ideaCBC_ISOpad', '1.3.36.3.1.2.3': 'ideaOFB', '1.3.36.3.1.2.4': 'ideaCFB', '1.3.36.3.1.4': 'rsaEncryption', '1.3.36.3.1.4.512.17': 'rsaEncryptionWithlmod512expe17', '1.3.36.3.1.5': 'bsi-1', '1.3.36.3.1.5.1': 'bsi_1ECB_pad', '1.3.36.3.1.5.2': 'bsi_1CBC_pad', '1.3.36.3.1.5.2.1': 'bsi_1CBC_PEMpad', '1.3.36.3.2': 'hashAlgorithm', '1.3.36.3.2.1': 'ripemd160', '1.3.36.3.2.2': 'ripemd128', '1.3.36.3.2.3': 'ripemd256', '1.3.36.3.2.4': 'mdc2singleLength', '1.3.36.3.2.5': 'mdc2doubleLength', '1.3.36.3.3': 'signatureAlgorithm', '1.3.36.3.3.1': 'rsaSignature', '1.3.36.3.3.1.1': 'rsaSignatureWithsha1', '1.3.36.3.3.1.1.1024.11': 'rsaSignatureWithsha1_l1024_l11', '1.3.36.3.3.1.2': 'rsaSignatureWithripemd160', '1.3.36.3.3.1.2.1024.11': 'rsaSignatureWithripemd160_l1024_l11', '1.3.36.3.3.1.3': 'rsaSignatureWithrimpemd128', '1.3.36.3.3.1.4': 'rsaSignatureWithrimpemd256', '1.3.36.3.3.2': 'ecsieSign', '1.3.36.3.3.2.1': 'ecsieSignWithsha1', '1.3.36.3.3.2.2': 'ecsieSignWithripemd160', '1.3.36.3.3.2.3': 'ecsieSignWithmd2', '1.3.36.3.3.2.4': 'ecsieSignWithmd5', '2.5.4.0': 'objectClass', '2.5.4.1': 'aliasedEntryName', '2.5.4.2': 'knowledgeInformation', '2.5.4.3': 'commonName', '2.5.4.4': 'surname', '2.5.4.5': 'serialNumber', '2.5.4.6': 'countryName', '2.5.4.7': 'localityName', '2.5.4.7.1': 'collectiveLocalityName', '2.5.4.8': 'stateOrProvinceName', 
'2.5.4.8.1': 'collectiveStateOrProvinceName', '2.5.4.9': 'streetAddress', '2.5.4.9.1': 'collectiveStreetAddress', '2.5.4.10': 'organizationName', '2.5.4.10.1': 'collectiveOrganizationName', '2.5.4.11': 'organizationalUnitName', '2.5.4.11.1': 'collectiveOrganizationalUnitName', '2.5.4.12': 'title', '2.5.4.13': 'description', '2.5.4.14': 'searchGuide', '2.5.4.15': 'businessCategory', '2.5.4.16': 'postalAddress', '2.5.4.16.1': 'collectivePostalAddress', '2.5.4.17': 'postalCode', '2.5.4.17.1': 'collectivePostalCode', '2.5.4.18': 'postOfficeBox', '2.5.4.18.1': 'collectivePostOfficeBox', '2.5.4.19': 'physicalDeliveryOfficeName', '2.5.4.19.1': 'collectivePhysicalDeliveryOfficeName', '2.5.4.20': 'telephoneNumber', '2.5.4.20.1': 'collectiveTelephoneNumber', '2.5.4.21': 'telexNumber', '2.5.4.21.1': 'collectiveTelexNumber', '2.5.4.22': 'teletexTerminalIdentifier', '2.5.4.22.1': 'collectiveTeletexTerminalIdentifier', '2.5.4.23': 'facsimileTelephoneNumber', '2.5.4.23.1': 'collectiveFacsimileTelephoneNumber', '2.5.4.24': 'x121Address', '2.5.4.25': 'internationalISDNNumber', '2.5.4.25.1': 'collectiveInternationalISDNNumber', '2.5.4.26': 'registeredAddress', '2.5.4.27': 'destinationIndicator', '2.5.4.28': 'preferredDeliveryMehtod', '2.5.4.29': 'presentationAddress', '2.5.4.30': 'supportedApplicationContext', '2.5.4.31': 'member', '2.5.4.32': 'owner', '2.5.4.33': 'roleOccupant', '2.5.4.34': 'seeAlso', '2.5.4.35': 'userPassword', '2.5.4.36': 'userCertificate', '2.5.4.37': 'caCertificate', '2.5.4.38': 'authorityRevocationList', '2.5.4.39': 'certificateRevocationList', '2.5.4.40': 'crossCertificatePair', '2.5.4.41': 'name', '2.5.4.42': 'givenName', '2.5.4.43': 'initials', '2.5.4.44': 'generationQualifier', '2.5.4.45': 'uniqueIdentifier', '2.5.4.46': 'dnQualifier', '2.5.4.47': 'enhancedSearchGuide', '2.5.4.48': 'protocolInformation', '2.5.4.49': 'distinguishedName', '2.5.4.50': 'uniqueMember', '2.5.4.51': 'houseIdentifier', '2.5.4.52': 'supportedAlgorithms', '2.5.4.53': 'deltaRevocationList', '2.5.4.54': 'dmdName', '2.5.4.55': 'clearance', '2.5.4.56': 'defaultDirQop', '2.5.4.57': 'attributeIntegrityInfo', '2.5.4.58': 'attributeCertificate', '2.5.4.59': 'attributeCertificateRevocationList', '2.5.4.60': 'confKeyInfo', '2.5.4.61': 'aACertificate', '2.5.4.62': 'attributeDescriptorCertificate', '2.5.4.63': 'attributeAuthorityRevocationList', '2.5.4.64': 'familyInformation', '2.5.4.65': 'pseudonym', '2.5.4.66': 'communicationsService', '2.5.4.67': 'communicationsNetwork', '2.5.4.68': 'certificationPracticeStmt', '2.5.4.69': 'certificatePolicy', '2.5.4.70': 'pkiPath', '2.5.4.71': 'privPolicy', '2.5.4.72': 'role', '2.5.4.73': 'delegationPath', '2.5.4.74': 'protPrivPolicy', '2.5.4.75': 'xMLPrivilegeInfo', '2.5.4.76': 'xmlPrivPolicy', '2.5.4.82': 'permission', '2.5.6.0': 'top', '2.5.6.1': 'alias', '2.5.6.2': 'country', '2.5.6.3': 'locality', '2.5.6.4': 'organization', '2.5.6.5': 'organizationalUnit', '2.5.6.6': 'person', '2.5.6.7': 'organizationalPerson', '2.5.6.8': 'organizationalRole', '2.5.6.9': 'groupOfNames', '2.5.6.10': 'residentialPerson', '2.5.6.11': 'applicationProcess', '2.5.6.12': 'applicationEntity', '2.5.6.13': 'dSA', '2.5.6.14': 'device', '2.5.6.15': 'strongAuthenticationUser', '2.5.6.16': 'certificateAuthority', '2.5.6.17': 'groupOfUniqueNames', '2.5.6.21': 'pkiUser', '2.5.6.22': 'pkiCA', '2.5.8.1.1': 'rsa', '2.5.29.1': 'authorityKeyIdentifier', '2.5.29.2': 'keyAttributes', '2.5.29.3': 'certificatePolicies', '2.5.29.4': 'keyUsageRestriction', '2.5.29.5': 'policyMapping', '2.5.29.6': 'subtreesConstraint', '2.5.29.7': 'subjectAltName', '2.5.29.8': 'issuerAltName', '2.5.29.9': 'subjectDirectoryAttributes', '2.5.29.10': 'basicConstraints', '2.5.29.11': 'nameConstraints', '2.5.29.12': 'policyConstraints', '2.5.29.13': 'basicConstraints', '2.5.29.14': 'subjectKeyIdentifier', '2.5.29.15': 'keyUsage', '2.5.29.16': 'privateKeyUsagePeriod', '2.5.29.17': 'subjectAltName', '2.5.29.18': 'issuerAltName', '2.5.29.19': 'basicConstraints', '2.5.29.20': 'cRLNumber', '2.5.29.21': 'cRLReason', '2.5.29.22': 'expirationDate', '2.5.29.23': 'instructionCode', '2.5.29.24': 'invalidityDate', '2.5.29.25': 'cRLDistributionPoints', '2.5.29.26': 'issuingDistributionPoint', '2.5.29.27': 'deltaCRLIndicator', '2.5.29.28': 'issuingDistributionPoint', '2.5.29.29': 'certificateIssuer', '2.5.29.30': 'nameConstraints', '2.5.29.31': 'cRLDistributionPoints', '2.5.29.32': 'certificatePolicies', '2.5.29.32.0': 'anyPolicy', '2.5.29.33': 'policyMappings', '2.5.29.34': 'policyConstraints', '2.5.29.35': 'authorityKeyIdentifier', '2.5.29.36': 'policyConstraints', '2.5.29.37': 'extKeyUsage', '2.5.29.37.0': 'anyExtendedKeyUsage', '2.5.29.38': 'authorityAttributeIdentifier', '2.5.29.39': 'roleSpecCertIdentifier', '2.5.29.40': 'cRLStreamIdentifier', '2.5.29.41': 'basicAttConstraints', '2.5.29.42': 'delegatedNameConstraints', '2.5.29.43': 'timeSpecification', '2.5.29.44': 'cRLScope', '2.5.29.45': 'statusReferrals', '2.5.29.46': 'freshestCRL', '2.5.29.47': 'orderedList', '2.5.29.48': 'attributeDescriptor', '2.5.29.49': 'userNotice', '2.5.29.50': 'sOAIdentifier', '2.5.29.51': 'baseUpdateTime', '2.5.29.52': 'acceptableCertPolicies', '2.5.29.53': 'deltaInfo', '2.5.29.54': 'inhibitAnyPolicy', '2.5.29.55': 'targetInformation', '2.5.29.56': 'noRevAvail', '2.5.29.57': 'acceptablePrivilegePolicies', '2.5.29.58': 'toBeRevoked', '2.5.29.59': 'revokedGroups', '2.5.29.60': 'expiredCertsOnCRL', '2.5.29.61': 'indirectIssuer', '2.5.29.62': 'noAssertion', '2.5.29.63': 'aAissuingDistributionPoint', '2.5.29.64': 'issuedOnBehalfOf', '2.5.29.65': 'singleUse', '2.5.29.66': 'groupAC', '2.5.29.67': 'allowedAttAss', '2.5.29.68': 'attributeMappings', '2.5.29.69': 'holderNameConstraints', '2.16.840.1.101.2.1.22': 'errorCodes', '2.16.840.1.101.2.1.22.1': 'missingKeyType', '2.16.840.1.101.2.1.22.2': 'privacyMarkTooLong', '2.16.840.1.101.2.1.22.3': 'unrecognizedSecurityPolicy', '2.16.840.1.101.3.1': 'slabel', '2.16.840.1.101.3.2': 'pki', '2.16.840.1.101.3.2.1': 'NIST policyIdentifier', '2.16.840.1.101.3.2.1.3.1': 'fbcaRudimentaryPolicy', '2.16.840.1.101.3.2.1.3.2': 'fbcaBasicPolicy', '2.16.840.1.101.3.2.1.3.3': 'fbcaMediumPolicy', '2.16.840.1.101.3.2.1.3.4': 'fbcaHighPolicy', '2.16.840.1.101.3.2.2': 'gak', '2.16.840.1.101.3.2.2.1': 'kRAKey', '2.16.840.1.101.3.2.3': 'extensions', '2.16.840.1.101.3.2.3.1': 'kRTechnique', '2.16.840.1.101.3.2.3.2': 'kRecoveryCapable', '2.16.840.1.101.3.2.3.3': 'kR', '2.16.840.1.101.3.2.4': 'keyRecoverySchemes', '2.16.840.1.101.3.2.5': 'krapola', '2.16.840.1.101.3.3': 'arpa', '2.23.42.0': 'contentType', '2.23.42.0.0': 'panData', '2.23.42.0.1': 'panToken', '2.23.42.0.2': 'panOnly', '2.23.42.1': 'msgExt', '2.23.42.2': 'field', '2.23.42.2.0': 'fullName', '2.23.42.2.1': 'givenName', '2.23.42.2.2': 'familyName', '2.23.42.2.3': 'birthFamilyName', '2.23.42.2.4': 'placeName', '2.23.42.2.5': 'identificationNumber', '2.23.42.2.6': 'month', '2.23.42.2.7': 'date', '2.23.42.2.8': 'address', '2.23.42.2.9': 'telephone', '2.23.42.2.10': 'amount', '2.23.42.2.11': 'accountNumber', '2.23.42.2.12': 'passPhrase', '2.23.42.3': 'attribute', '2.23.42.3.0': 'cert', '2.23.42.3.0.0': 'rootKeyThumb', '2.23.42.3.0.1': 'additionalPolicy', '2.23.42.4': 'algorithm', '2.23.42.5': 'policy', '2.23.42.5.0': 'root', '2.23.42.6': 'module', '2.23.42.7': 'certExt', '2.23.42.7.0': 'hashedRootKey', '2.23.42.7.1': 'certificateType', '2.23.42.7.2': 'merchantData', '2.23.42.7.3': 'cardCertRequired', '2.23.42.7.4': 'tunneling', '2.23.42.7.5': 'setExtensions', '2.23.42.7.6': 'setQualifier', '2.23.42.8': 'brand'
}
}
var oids = get_oids(),
    ellipsis = "\u2026",
    reTimeS =     /^(\d\d)(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])([01]\d|2[0-3])(?:([0-5]\d)(?:([0-5]\d)(?:[.,](\d{1,3}))?)?)?(Z|[-+](?:[0]\d|1[0-2])([0-5]\d)?)?$/,
    reTimeL = /^(\d\d\d\d)(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])([01]\d|2[0-3])(?:([0-5]\d)(?:([0-5]\d)(?:[.,](\d{1,3}))?)?)?(Z|[-+](?:[0]\d|1[0-2])([0-5]\d)?)?$/;

function stringCut(str, len) {
    if (str.length > len)
        str = str.substring(0, len) + ellipsis;
    return str;
}

function Stream(enc, pos) {
    if (enc instanceof Stream) {
        this.enc = enc.enc;
        this.pos = enc.pos;
    } else {
        this.enc = enc;
        this.pos = pos;
    }
}
Stream.prototype.get = function (pos) {
    if (pos === undefined)
        pos = this.pos++;
    if (pos >= this.enc.length)
        throw 'Requesting byte offset ' + pos + ' on a stream of length ' + this.enc.length;
    return (typeof this.enc == "string") ? this.enc.charCodeAt(pos) : this.enc[pos];
};
Stream.prototype.hexDigits = "0123456789ABCDEF";
Stream.prototype.hexByte = function (b) {
    return this.hexDigits.charAt((b >> 4) & 0xF) + this.hexDigits.charAt(b & 0xF);
};
Stream.prototype.hexDump = function (start, end, raw) {
    var s = "";
    for (var i = start; i < end; ++i) {
        s += this.hexByte(this.get(i));
        if (raw !== true)
            switch (i & 0xF) {
            case 0x7: s += "  "; break;
            case 0xF: s += "\n"; break;
            default:  s += " ";
            }
    }
    return s;
};
var b64Safe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
Stream.prototype.b64Dump = function (start, end) {
    var extra = (end - start) % 3,
        s = '',
        i, c;
    for (i = start; i + 2 < end; i += 3) {
        c = this.get(i) << 16 | this.get(i + 1) << 8 | this.get(i + 2);
        s += b64Safe.charAt(c >> 18 & 0x3F);
        s += b64Safe.charAt(c >> 12 & 0x3F);
        s += b64Safe.charAt(c >>  6 & 0x3F);
        s += b64Safe.charAt(c       & 0x3F);
    }
    if (extra > 0) {
        c = this.get(i) << 16;
        if (extra > 1) c |= this.get(i + 1) << 8;
        s += b64Safe.charAt(c >> 18 & 0x3F);
        s += b64Safe.charAt(c >> 12 & 0x3F);
        if (extra == 2) s += b64Safe.charAt(c >> 6 & 0x3F);
    }
    return s;
};
Stream.prototype.isASCII = function (start, end) {
    for (var i = start; i < end; ++i) {
        var c = this.get(i);
        if (c < 32 || c > 176)
            return false;
    }
    return true;
};
Stream.prototype.parseStringISO = function (start, end) {
    var s = "";
    for (var i = start; i < end; ++i)
        s += String.fromCharCode(this.get(i));
    return s;
};
Stream.prototype.parseStringUTF = function (start, end) {
    function ex(c) {
        if ((c < 0x80) || (c >= 0xC0))
            throw new Error('Invalid UTF-8 continuation byte: ' + c);
        return (c & 0x3F);
    }
    function surrogate(cp) {
        if (cp < 0x10000)
            throw new Error('UTF-8 overlong encoding, codepoint encoded in 4 bytes: ' + cp);
        cp -= 0x10000;
        return String.fromCharCode((cp >> 10) + 0xD800, (cp & 0x3FF) + 0xDC00);
    }
    var s = "";
    for (var i = start; i < end; ) {
        var c = this.get(i++);
        if (c < 0x80)
            s += String.fromCharCode(c);
        else if (c < 0xC0)
            throw new Error('Invalid UTF-8 starting byte: ' + c);
        else if (c < 0xE0)
            s += String.fromCharCode(((c & 0x1F) << 6) | ex(this.get(i++)));
        else if (c < 0xF0)
            s += String.fromCharCode(((c & 0x0F) << 12) | (ex(this.get(i++)) << 6) | ex(this.get(i++)));
        else if (c < 0xF8)
            s += surrogate(((c & 0x07) << 18) | (ex(this.get(i++)) << 12) | (ex(this.get(i++)) << 6) | ex(this.get(i++)));
        else
            throw new Error('Invalid UTF-8 starting byte (since 2003 it is restricted to 4 bytes): ' + c);
    }
    return s;
};
Stream.prototype.parseStringBMP = function (start, end) {
    var str = "", hi, lo;
    for (var i = start; i < end; ) {
        hi = this.get(i++);
        lo = this.get(i++);
        str += String.fromCharCode((hi << 8) | lo);
    }
    return str;
};
Stream.prototype.parseTime = function (start, end, shortYear) {
    var s = this.parseStringISO(start, end),
        m = (shortYear ? reTimeS : reTimeL).exec(s);
    if (!m)
        return "Unrecognized time: " + s;
    if (shortYear) {
        m[1] = +m[1];
        m[1] += (m[1] < 70) ? 2000 : 1900;
    }
    s = m[1] + "-" + m[2] + "-" + m[3] + " " + m[4];
    if (m[5]) {
        s += ":" + m[5];
        if (m[6]) {
            s += ":" + m[6];
            if (m[7])
                s += "." + m[7];
        }
    }
    if (m[8]) {
        s += " UTC";
        if (m[8] != 'Z') {
            s += m[8];
            if (m[9])
                s += ":" + m[9];
        }
    }
    return s;
};
Stream.prototype.parseInteger = function (start, end) {
    var v = this.get(start),
        neg = (v > 127),
        pad = neg ? 255 : 0,
        len,
        s = '';
    while (v == pad && ++start < end)
        v = this.get(start);
    len = end - start;
    if (len === 0)
        return neg ? '-1' : '0';
    if (len > 4) {
        s = v;
        len <<= 3;
        while (((s ^ pad) & 0x80) == 0) {
            s <<= 1;
            --len;
        }
    }
    if (neg) v = v - 256;
    var n = new Int10(v);
    for (var i = start + 1; i < end; ++i)
        n.mulAdd(256, this.get(i));
    return s + n.toString();
};
Stream.prototype.parseBitString = function (start, end, maxLength) {
    var unusedBits = this.get(start);
    if (unusedBits > 7)
        throw 'Invalid BitString with unusedBits=' + unusedBits;
    var lenBit = ((end - start - 1) << 3) - unusedBits,
        s = "";
    for (var i = start + 1; i < end; ++i) {
        var b = this.get(i),
            skip = (i == end - 1) ? unusedBits : 0;
        for (var j = 7; j >= skip; --j)
            s += (b >> j) & 1 ? "1" : "0";
        if (s.length > maxLength)
            s = stringCut(s, maxLength);
    }
    return { size: lenBit, str: s };
};
Stream.prototype.parseOctetString = function (start, end, maxLength) {
    var len = end - start,
        s;
    try {
        s = this.parseStringUTF(start, end);
        var v;
        for (i = 0; i < s.length; ++i) {
            v = s.charCodeAt(i);
            if (v < 32 && v != 9 && v != 10 && v != 13)
                throw new Error('Unprintable character at index ' + i + ' (code ' + s.charCodeAt(i) + ")");
        }
        return { size: len, str: s };
    } catch (e) {
    }
    maxLength /= 2;
    if (len > maxLength)
        end = start + maxLength;
    s = '';
    for (var i = start; i < end; ++i)
        s += this.hexByte(this.get(i));
    if (len > maxLength)
        s += ellipsis;
    return { size: len, str: s };
};
Stream.prototype.parseOID = function (start, end, maxLength) {
    var s = '',
        n = new Int10(),
        bits = 0;
    for (var i = start; i < end; ++i) {
        var v = this.get(i);
        n.mulAdd(128, v & 0x7F);
        bits += 7;
        if (!(v & 0x80)) {
            if (s === '') {
                n = n.simplify();
                if (n instanceof Int10) {
                    n.sub(80);
                    s = "2." + n.toString();
                } else {
                    var m = n < 80 ? n < 40 ? 0 : 1 : 2;
                    s = m + "." + (n - m * 40);
                }
            } else
                s += "." + n.toString();
            if (s.length > maxLength)
                return stringCut(s, maxLength);
            n = new Int10();
            bits = 0;
        }
    }
    if (bits > 0)
        s += ".incomplete";
    if (typeof oids === 'object') {
        var oid = oids[s];
        if (oid) {
            s += "\n" + oid;
        } else {
			s = "\n" + s;
		}
    }

    return s;
};

function ASN1(stream, header, length, tag, tagLen, sub) {
    if (!(tag instanceof ASN1Tag)) throw 'Invalid tag value.';
    this.stream = stream;
    this.header = header;
    this.length = length;
    this.tag = tag;
    this.tagLen = tagLen;
    this.sub = sub;
}
ASN1.prototype.typeName = function () {
    switch (this.tag.tagClass) {
    case 0: // universal
        switch (this.tag.tagNumber) {
        case 0x00: return "EOC";
        case 0x01: return "BOOLEAN";
        case 0x02: return "INTEGER";
        case 0x03: return "BIT_STRING";
        case 0x04: return "OCTET_STRING";
        case 0x05: return "NULL";
        case 0x06: return "OBJECT_IDENTIFIER";
        case 0x07: return "ObjectDescriptor";
        case 0x08: return "EXTERNAL";
        case 0x09: return "REAL";
        case 0x0A: return "ENUMERATED";
        case 0x0B: return "EMBEDDED_PDV";
        case 0x0C: return "UTF8String";
        case 0x10: return "SEQUENCE";
        case 0x11: return "SET";
        case 0x12: return "NumericString";
        case 0x13: return "PrintableString";
        case 0x14: return "TeletexString";
        case 0x15: return "VideotexString";
        case 0x16: return "IA5String";
        case 0x17: return "UTCTime";
        case 0x18: return "GeneralizedTime";
        case 0x19: return "GraphicString";
        case 0x1A: return "VisibleString";
        case 0x1B: return "GeneralString";
        case 0x1C: return "UniversalString";
        case 0x1E: return "BMPString";
        }
        return "Universal_" + this.tag.tagNumber.toString();
    case 1: return "Application_" + this.tag.tagNumber.toString();
    case 2: return "[" + this.tag.tagNumber.toString() + "]";
    case 3: return "Private_" + this.tag.tagNumber.toString();
    }
};
function recurse(el, parser, maxLength) {
    var differentTags = false;
    if (el.sub) el.sub.forEach(function (e1) {
        if (e1.tag.tagClass != el.tag.tagClass || e1.tag.tagNumber != el.tag.tagNumber)
            differentTags = true;
    });
    if (!el.sub || differentTags)
        return el.stream[parser](el.posContent(), el.posContent() + Math.abs(el.length), maxLength);
    var d = { size: 0, str: '' };
    el.sub.forEach(function (el) {
        var d1 = recurse(el, parser, maxLength - d.str.length);
        d.size += d1.size;
        d.str += d1.str;
    });
    return d;
}

function hex_with_colons (hex) {

	if(hex.length % 2 != 0) {
		hex = "0" + hex
	}
	return hex.replace(/(..)/g, '$1:').slice(0,-1);
}

/** A string preview of the content (intended for humans). */
ASN1.prototype.content = function (maxLength) {
    if (this.tag === undefined)
        return null;
    if (maxLength === undefined)
        maxLength = Infinity;
    var content = this.posContent(),
        len = Math.abs(this.length);
    if (!this.tag.isUniversal()) {
        if (this.sub !== null)
            return "";
        var d1 = this.stream.parseOctetString(content, content + len, maxLength);
        return d1.str;
    }
    switch (this.tag.tagNumber) {
    case 0x01:
        return (this.stream.get(content) === 0) ? "false" : "true";
    case 0x02:
        return this.stream.parseInteger(content, content + len);
    case 0x03:
        var d = recurse(this, 'parseOctetString', maxLength);
        return hex_with_colons(d.str);
    case 0x04:
        d = recurse(this, 'parseOctetString', maxLength);
        return hex_with_colons(d.str);
    case 0x06:
        return this.stream.parseOID(content, content + len, maxLength).split("\n")[1] + ": ";
    case 0x0A:
        return this.stream.parseInteger(content, content + len);
    case 0x10:
    case 0x11:
        return "";
    case 0x0C:
        return stringCut(this.stream.parseStringUTF(content, content + len), maxLength);
    case 0x12:
    case 0x13:
    case 0x14:
    case 0x15:
    case 0x16:
    case 0x1A:
    case 0x1B:
        return stringCut(this.stream.parseStringISO(content, content + len), maxLength);
    case 0x1E:
        return stringCut(this.stream.parseStringBMP(content, content + len), maxLength);
    case 0x17:
    case 0x18:
        return "Time: " + this.stream.parseTime(content, content + len, (this.tag.tagNumber == 0x17));
    }
    return null;
};
ASN1.prototype.toString = function () {
    return this.typeName() + "@" + this.stream.pos + "[header:" + this.header + ",length:" + this.length + ",sub:" + ((this.sub === null) ? 'null' : this.sub.length) + "]";
};
ASN1.prototype.toPrettyString = function (indent) {
    var s = "";
    var content = this.content();
    if (content)
        s += content.replace(/\n/g, '|');

	if(!s.endsWith(": ") && !s.endsWith(":") && content){
		s += "\n";
	}

    if (this.sub !== null) {
        for (var i = 0, max = this.sub.length; i < max; ++i)
            s += this.sub[i].toPrettyString();
    }
	
    return s;
};
ASN1.prototype.posStart = function () {
    return this.stream.pos;
};
ASN1.prototype.posContent = function () {
    return this.stream.pos + this.header;
};
ASN1.prototype.posEnd = function () {
    return this.stream.pos + this.header + Math.abs(this.length);
};
/** Position of the length. */
ASN1.prototype.posLen = function() {
    return this.stream.pos + this.tagLen;
};
ASN1.prototype.toHexString = function () {
    return this.stream.hexDump(this.posStart(), this.posEnd(), true);
};
ASN1.prototype.toB64String = function () {
    return this.stream.b64Dump(this.posStart(), this.posEnd());
};
ASN1.decodeLength = function (stream) {
    var buf = stream.get(),
        len = buf & 0x7F;
    if (len == buf)
        return len;
    if (len === 0)
        return null;
    if (len > 6)
        throw "Length over 48 bits not supported at position " + (stream.pos - 1);
    buf = 0;
    for (var i = 0; i < len; ++i)
        buf = (buf * 256) + stream.get();
    return buf;
};
function ASN1Tag(stream) {
    var buf = stream.get();
    this.tagClass = buf >> 6;
    this.tagConstructed = ((buf & 0x20) !== 0);
    this.tagNumber = buf & 0x1F;
    if (this.tagNumber == 0x1F) {
        var n = new Int10();
        do {
            buf = stream.get();
            n.mulAdd(128, buf & 0x7F);
        } while (buf & 0x80);
        this.tagNumber = n.simplify();
    }
}
ASN1Tag.prototype.isUniversal = function () {
    return this.tagClass === 0x00;
};
ASN1Tag.prototype.isEOC = function () {
    return this.tagClass === 0x00 && this.tagNumber === 0x00;
};
ASN1.decode = function (stream, offset) {
    if (!(stream instanceof Stream))
        stream = new Stream(stream, offset || 0);
    var streamStart = new Stream(stream),
        tag = new ASN1Tag(stream),
        tagLen = stream.pos - streamStart.pos,
        len = ASN1.decodeLength(stream),
        start = stream.pos,
        header = start - streamStart.pos,
        sub = null,
        getSub = function () {
            sub = [];
            if (len !== null) {
                var end = start + len;
                if (end > stream.enc.length)
                    throw 'Container at offset ' + start +  ' has a length of ' + len + ', which is past the end of the stream';
                while (stream.pos < end)
                    sub[sub.length] = ASN1.decode(stream);
                if (stream.pos != end)
                    throw 'Content size is not correct for container at offset ' + start;
            } else {
                try {
                    for (;;) {
                        var s = ASN1.decode(stream);
                        if (s.tag.isEOC())
                            break;
                        sub[sub.length] = s;
                    }
                    len = start - stream.pos;
                } catch (e) {
                    throw 'Exception while decoding undefined length content at offset ' + start + ': ' + e;
                }
            }
        };
    if (tag.tagConstructed) {
        getSub();
    } else if (tag.isUniversal() && ((tag.tagNumber == 0x03) || (tag.tagNumber == 0x04))) {
        try {
            if (tag.tagNumber == 0x03)
                if (stream.get() != 0)
                    throw "BIT STRINGs with unused bits cannot encapsulate.";
            getSub();
            for (var i = 0; i < sub.length; ++i)
                if (sub[i].tag.isEOC())
                    throw 'EOC is not supposed to be actual content.';
        } catch (e) {
            sub = null;
        }
    }
    if (sub === null) {
        if (len === null)
            throw "We can't skip over an invalid tag with undefined length at offset " + start;
        stream.pos = start + Math.abs(len);
    }
    return new ASN1(streamStart, header, len, tag, tagLen, sub);
};