/*
Copyright (c) Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package main

import (
	"encoding/json"
	"log"

	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"

	"gitlab.com/perinet/generic/apiservice/dnssd"
	"gitlab.com/perinet/generic/apiservice/staticfiles"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	"gitlab.com/perinet/periMICA-container/apiservice/mosquitto"
	"gitlab.com/perinet/periMICA-container/apiservice/network"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/security"

	"gitlab.com/perinet/generic/lib/httpserver"
)

var (
	logger    log.Logger = *log.Default()
	nodeEvent            = func() {
		setDnssdAdv()
	}
	securityEvent = func() {
		setHttpSecurity()
		httpserver.Restart()
	}
)

func init() {
	logger.SetPrefix("Service MqttContainer: ")

	logger.Println("Initializing")
	var data []byte
	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "network", "dns-sd", "mqttbroker"}
	data, _ = json.Marshal(services)
	intHttp.Put(node.ServicesSet, data, nil)

	node.RegisterNodeEventCallback(nodeEvent)
	security.RegisterSecurityEventCallback(securityEvent)

	var err error
	data = intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())
	httpserver.AddPaths(network.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(mosquitto.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(staticfiles.PathsGet())

	mosquitto.SetServiceConfig(mosquitto.ServiceConfig{BrokerRestartCallback: restartBroker})
}

func restartBroker() bool {
	result := shellhelper.CommandCall("/etc/container-service/mqttbroker/restart.sh")
	if result.Exitcode != 0 {
		logger.Printf("Executing broker restart command returned exit code: %d\n", result.Exitcode)
		return false
	}
	return true
}

func setDnssdAdv() {
	var data []byte
	var err error

	data = intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	data = intHttp.Get(node.ProductionInfoGet, nil)
	var nodeProductionInfo node.ProductionInfo
	err = json.Unmarshal(data, &nodeProductionInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeProductionInfo: ", err.Error())
	}

	//start advertising _https via dnssd
	dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: "_https._tcp", Port: 443}
	txt_record := dnssd.DNSSDTxtRecordHttps{Format: "uAPI"}
	txt_record.ApiVersion = nodeInfo.ApiVersion
	txt_record.ApplicationName = nodeInfo.Config.ApplicationName
	txt_record.ElementName = nodeInfo.Config.ElementName
	txt_record.ProductName = nodeProductionInfo.ProductName
	txt_record.ErrorStatus = nodeInfo.ErrorStatus
	dnssdServiceInfo.TxtRecord = txt_record
	data, _ = json.Marshal(dnssdServiceInfo)
	intHttp.Put(dnssd.DNSSDServiceInfoAdvertiseSet, data, map[string]string{"service_name": dnssdServiceInfo.ServiceName})
}

func setHttpSecurity() {
	var securityConfig security.SecurityConfig
	data := intHttp.Get(security.Security_Config_Get, nil)
	err := json.Unmarshal(data, &securityConfig)
	if err != nil {
		logger.Println("error getting security config: ", err)
		return
	}

	auth.SetAuthMethod(securityConfig.ClientAuthMethod)

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      intHttp.Get(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    intHttp.Get(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: intHttp.Get(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})
}

func main() {
	logger.Println("Starting")

	nodeEvent()
	securityEvent()

	httpserver.HttpRedirect("[::]:80", 443)
	httpserver.ListenAndServe("[::]:443")
}
