/*
Copyright (c) Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package main

import (
	"testing"

	"gitlab.com/perinet/generic/apiservice/staticfiles"
	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gitlab.com/perinet/periMICA-container/apiservice/mosquitto"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
)

func init() {
	staticfiles.Set_files_path("ressources/webui")

	auth.SetAuthMethod(auth.NONE)
	node.SetServiceConfig(node.ServiceConfig{StorageBasePath: "_cache/var/lib/apiservice-node"})
	security.SetServiceConfig(security.ServiceConfig{StorageBasePath: "_cache/var/lib/apiservice-security"})

	shellhelper.WriteFile("_cache/var/log/mosquitto.log", "")
	mosquitto.SetServiceConfig(mosquitto.ServiceConfig{
		StoragePath:             "_cache/var/lib/apiservice-mosquitto",
		ListenerConfigPath:      "_cache/etc/mosquitto/conf.d/00_mqtt-listener.conf",
		MosquittoUserConfigPath: "_cache/etc/mosquitto/conf.d/conf.d/user.conf",
		MosquittoLogFilePath:    "_cache/var/log/mosquitto.log",
	})
}

func TestMain(t *testing.T) {
	logger.Println("Starting")

	nodeEvent()
	securityEvent()

	// webhelper.InternalPatch(security.Security_Config_Set, []byte("{\"client_auth_method\":\"mTLS\"}"))
	// webhelper.InternalPatch(security.Security_Commit_Set, []byte(""))

	httpserver.HttpRedirect("[::]:80", 50443)
	httpserver.ListenAndServe("[::1]:50443")
}
